//
//  main.m
//  CC2013Push
//
//  Created by liu zhiliang on 14-4-11.
//  Copyright (c) 2014年 Jee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
