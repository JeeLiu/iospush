//
//  AppDelegate.h
//  CC2013Push
//
//  Created by liu zhiliang on 14-4-11.
//  Copyright (c) 2014年 Jee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *deviceToken;
+(id)shareAppDelegate;

@end
