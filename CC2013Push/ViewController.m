//
//  ViewController.m
//  CC2013Push
//
//  Created by liu zhiliang on 14-4-11.
//  Copyright (c) 2014年 Jee. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)loadDeviceToken
{
    NSLog(@"load deviceToken = %@",[[AppDelegate shareAppDelegate] deviceToken]);
    self.label.text = [[AppDelegate shareAppDelegate] deviceToken];
}


- (void)loadContent:(NSDictionary *)notificationInfo
{
    NSLog(@"content = %@",notificationInfo);
    self.contentLabel.text = notificationInfo.description;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
