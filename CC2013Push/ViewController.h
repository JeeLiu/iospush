//
//  ViewController.h
//  CC2013Push
//
//  Created by liu zhiliang on 14-4-11.
//  Copyright (c) 2014年 Jee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) NSString *textName;
- (void)loadDeviceToken;

- (void)loadContent:(NSDictionary *)notificationInfo;

@end
